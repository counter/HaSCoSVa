# HaSCoSVa
This repository contains the (anonymized) Hate Speech Corpus with Spanish Variations (HaSCoSVa-2022) dataset. Keywords used for data collection as well as annotation guidelines are also included.

This dataset is released as a result of the work titled **Analyzing Zero-Shot transfer Scenarios across Spanish variants for Hate Speech Detection**, by Galo Castillo-López, Arij Riabi and Djamé Seddah. 

Work accepted for publication at [the Tenth Workshop on NLP for Similar Languages, Varieties and Dialects @EACL 2023](https://sites.google.com/view/vardial-2023/home?authuser=0). [Paper available at the ACL Anthology](https://aclanthology.org/2023.vardial-1.1/).


# Contacts
Galo Castillo-López (gadacast@fiec.espol.edu.ec)

Arij Riabi (arij.riabi@inria.fr)

Djamé Seddahh (djame.seddah@inria.fr)


